#initial time:0.343
#after index:0.328
#after PK:0.078


use mydb;

select * from proteins;
load data local infile  'C:/insert.txt' into table proteins fields terminated by '|';

select *
from proteins 
where protein_name like "%tumor%" and uniprot_id like "%human%"
order by uniprot_id;

create index uniprot_index on proteins(uniprot_id);
drop index uniprot_index on proteins;

alter table proteins add constraint pk_proteins primary key(uniprot_id);
alter table proteins drop primary key;

 
